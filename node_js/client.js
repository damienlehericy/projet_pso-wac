(function($){
	var socket = io.connect('http://localhost:3300');
	var msgtpl = $('#msgtpl').html();
	$('#msgtpl').remove();

	$('#loginform').submit(function(event){
		event.preventDefault();
		socket.emit('login', {
			username : $('#username').val(),
			mail : $('#exampleInputEmail1').val()
		});
	});

	socket.on('logged', function(){
		$('#popup').fadeOut();
		$('#message').focus();
	});

	/*  Envoie de messages */
	$('#form').submit(function(event){
		event.preventDefault();
		socket.emit('newmsg', {message: $('#message').val()});
		$('#message').val('');
		$('#message').focus();
	});

	socket.on('newmsg', function(message){
		$('#messages').append('<div class="message alert alert-info">' + Mustache.render(msgtpl, message) + '</div>');
		$('#messages').animate({scrollTop : $('#messages').prop('scrollHeight')}, 300);
	});

	/* Gestion des utilisateurs connecté */

	//Affiche les infos des users connecté
	socket.on('newusr', function(user){
		$('#user').append('<div class="dropdown" id="' + user.id + '"><img class="img-rounded img-responsive" src="' + user.avatar + '"><a data-toggle="dropdown" class="dropdown-toggle" href="#">' + user.username + '</a></div>');
		$('#' + user.id + '').append('<ul class="dropdown-menu dropdown-inverse dropuser" role="menu" aria-labelledby="dLabel"><li><a href="#">Message Priv&eacute;e</a></li><li><a href="#">Inviter dans une partie</a></li></ul>');
	});

	//Deconnexion de la chatroom
	socket.on('disusr', function(user){
		$('#' + user.id).remove();
	})

})(jQuery);