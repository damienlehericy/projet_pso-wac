var http = require('http');
var md5 = require('MD5');


var server = http.createServer(function(req, res) {
  res.writeHead(200);
  console.log('Un utilisateur affiche la page');
});
server.listen(3300);

//Ecoute du serveur par socket.io
var io = require('socket.io').listen(server);

//Variable qui stock les messages et users connecté
var users = {};
var messages = [];
var history = 3;

//Check connexion au serveur puis créer les variables users
io.sockets.on('connection', function(socket){
	console.log('new user');

	var me = false;

	for(var k in  users){
		socket.emit('newusr', users[k]);
	}

	for(var k in  messages){
		socket.emit('newmsg', messages[k]);
	}


	//Réception message
	socket.on('newmsg', function(message){
		message.user = me;
		date = new Date();
		message.h = date.getHours();
		message.m = date.getMinutes();
		messages.push(message);

		if(messages.length > history){
			messages.shift();
		}
		if(message.user.toString().substr(0,3) === '/w '){
			msg = message.user.substr(3);
			var ind = message.user.indexOf(' ');
			if(ind !== -1){
				var name = message.user.toString().substring(0, ind);
				var msg = msg.substring(ind + 1);
				if(name in users){
					users[name].emit('whisper', {message: msg, nick: socket.nickname});
					console.log('message sent is: ' + msg);
					console.log('Whisper!');
				} else{
					callback('Error!  Enter a valid user.');
				}
			} else{
				callback('Error!  Please enter a message for your whisper.');
			}
		}

		else{
			io.sockets.emit('newmsg', message);
		}
	});

	//Connexion
	socket.on('login', function(user){
		me = user;
		me.id = user.mail.replace('@', '-').replace('.','-');
		me.avatar = 'https://gravatar.com/avatar/'+ md5(user.mail) + '?s=50';
		socket.emit('logged');
		users[me.id] = me;
		io.sockets.emit('newusr', me);
	});

	//Déconnexion
	socket.on('disconnect', function(){
		if(!me){
			return false;
		}
		delete users[me.id];
		io.sockets.emit('disusr', me);
	})
});